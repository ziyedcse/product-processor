<?php
/**
 * Author: Md. Ziyed Uddin
 * Email: ziyed.cse@gmail.com
 * Upwork: https://www.upwork.com/freelancers/~01dee94459e713a64f
 * 
 * Project: A Product file parser to generate product objects 
 * 
 */

echo "Processing...\n\r";

$delimiters_array = [
    'comma' => ",",
    'tab' => "\t"
];
$params = getopt(null, ["file:", "format:", "delimiter:", "heading:", "unique-combinations:"]);

try {
    //File validation checking
    if(!isset( $params['file']) || empty( $params['file']) || !file_exists( $params['file']))
        throw new Exception("File is required"); 
        
    //File format validation checking
    if(!isset( $params['format']) || empty( $params['format']))
        throw new Exception("File format is required");     

    //Delimiter validation checking
    if(!isset( $params['delimiter']) || empty( $params['delimiter']) || !isset($delimiters_array[$params['delimiter']]))
        throw new Exception("Valid delimiter is required");     

    //Heading validation checking
    if(isset( $params['heading']) && !empty( $params['heading']))
        $keys = explode(',', $params['heading']);
    else 
        throw new Exception("Heading is required");    

    $isUniqueCombination = false;  
    $combinationArr = [];  
    if(isset($params['unique-combinations'])){
        $filename = $params['unique-combinations'];
        $isUniqueCombination = true;
    }

    switch ($params['format']) {
        case 'xml':
            # xml parser code goes here
            break;
    
        case 'json':
            # json parser code goes here
            break;
        
        default:
            # csv parser code goes here as default    
            $fp = fopen($params['file'], 'r');
            $skipFirstRow = true;

            while ( !feof($fp) )
            {               
                $line = fgets($fp);

                if($skipFirstRow){
                    $skipFirstRow = false;
                    continue;
                }

                $data = str_getcsv($line, $delimiters_array[$params['delimiter']]);                
                (count($keys) == count( $data)) && $dataObj = array_combine($keys, $data);
                print_r((object) $dataObj);

                if($isUniqueCombination){
                    $k = implode('_', str_replace(' ', '_', $data));
                    if(isset($combinationArr[$k])){
                        $combinationArr[$k]['count'] += 1 ;
                    }else{
                        is_array($dataObj) && $combinationArr[$k] = array_merge(['count' => 1], $dataObj);
                    }                    
                }
            } 
            
            fclose($fp);  

            if($isUniqueCombination){
                echo "Processing unique combination csv...\n";
                $fpw = fopen($filename, 'w');
                fputcsv($fpw, ['make', 'model', 'colour', 'capacity', 'network', 'grade', 'condition', 'count']);
                foreach ($combinationArr as $key => $value) {
                    fputcsv($fpw, [$value['make'], $value['model'], $value['colour'], $value['capacity'], $value['network'], $value['grade'], $value['condition'], $value['count']]);
                }
                fclose($fpw);
            }   
            
            echo "Done\n";
    
            break;
    }

} catch (\Exception $e) {
    echo 'Message: ' .$e->getMessage()."\n";
}




