# A product file processor

A product processor to process product csv file


The params for the shell script are give below
```
--file (source file name with path)
--format (file extension: csv/json/xml)
--delimiter (Delimiter: comma/tab)
--unique-combinations (unique combination output file name with path)
--heading (A comma separated list of product properties as per source document columns)
```

## How to run

1. Take pull the repo
2. Open your terminal and go to project folder
3. Make sure you have php globally installed 
4. run the parser file with all required parameters to parse (Please see the examples to see the command with params given below)


Example: 

Parse a csv file with comma separated delimiter
```
php parser.php --file=sources/products_comma_separated.csv --format=csv --delimiter=comma --unique-combinations=combination_count.csv --heading=make,model,condition,grade,capacity,colour,network
```

Parse a csv file with tab separated delimiter
```
php parser.php --file=sources/products_tab_separated.csv --format=csv --delimiter=tab --unique-combinations=combination_count.csv --heading=make,model,condition,grade,capacity,colour,network
```



